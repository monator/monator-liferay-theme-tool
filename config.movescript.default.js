var config = {};

config.copyList = [
	'css/aui.css',
	'css/custom.css',
	'css/_*',
	'css/partials',
	'templates',
	'js',
	'images/theme'
	];

module.exports = config;
