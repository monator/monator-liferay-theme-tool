var config				= {};
config.helper			= {};
config.settings			= {};
config.settings.onUp	= {};
config.settings.onBuild	= {};
config.settings.ide		= {};
config.path				= {};
config.path.source		= {};
config.path.server		= {};
config.file				= {};

/**
  * Helpers. These variables are just used for 
  * populating other variables in this config.
  */

config.helper.projectName				= 'project-name';
config.helper.themeName					= 'theme-name-theme';
config.helper.serverFolderName			= 'tomcat-7.0.27';
config.helper.serverBasePath			= '/Users/user/liferay';
config.helper.sourceBasePath			= '/Users/user/code';

/**
  * Settings
  */
config.settings.onBuild.command			= 'mvn clean package liferay:deploy -P monator-artifactory,' + config.helper.projectName; // Most commonly 'ant all' or 'mvn clean package liferay:deploy'.

config.settings.onUp.openIde			= true;
config.settings.onUp.runCssWatcher		= false;
config.settings.onUp.tailCatalina		= true;
config.settings.onUp.startServer		= true;

config.settings.onUp.startCommand		= 'startup.sh';
config.settings.onUp.stopCommand		= 'shutdown.sh';

config.settings.ide.command				= 'sublime';
config.settings.ide.projectArgument		= '--project';

/**
  * Paths to folders and files.
  */
config.path.server.liferay				= config.helper.serverBasePath + '/' + config.helper.projectName + '/' + config.helper.serverFolderName;
config.path.server.deploy				= config.path.server.liferay + '/../deploy';

config.path.source.themeRoot			= config.helper.sourceBasePath + '/' + config.helper.projectName + '/intra-sdk-6.1.20/themes/' + config.helper.themeName;
config.path.source.themeDiffs			= config.path.source.themeRoot + '/docroot/_diffs';

// The two lines above are default paths when using Liferay SDK. Use commented lines below if using Maven.
//config.path.source.themeRoot			= config.helper.sourceBasePath + '/' + config.helper.projectName + '/' + config.helper.themeName;
//config.path.source.themeDiffs			= config.path.source.themeRoot + '/src/main/webapp';

config.path.server.themeRoot			= config.helper.serverBasePath + '/' + config.helper.projectName  + '/' + config.helper.serverFolderName + '/webapps/' + config.helper.themeName;

config.file.WarFilename					= config.helper.themeName + '.war';

config.file.ideProject					= config.helper.sourceBasePath + '/sublime-projects/' + config.helper.projectName + '.sublime-project';
config.path.cssWatcher					= config.helper.sourceBasePath + '/my-tools/' + config.helper.projectName + '-css-toucher-custom';


// ...and export everything.
module.exports = config;
