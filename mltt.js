/*	COLORS. Usage: clc.colorname('output')
 *	black		bgBlack		blackBright		bgBlackBright	
 *	red			bgRed		redBright		bgRedBright	
 *	green		bgGreen		greenBright		bgGreenBright	
 *	yellow		bgYellow	yellowBright	bgYellowBright	
 *	blue		bgBlue		blueBright		bgBlueBright	
 *	magenta		bgMagenta	magentaBright	bgMagentaBright	
 *	cyan		bgCyan		cyanBright		bgCyanBright	
 *	white		bgWhite		whiteBright		bgWhiteBright
*/

// Load dependencies
var clc			= require('cli-color');
var fs			= require('fs-extra');
var glob		= require("glob");
var exec		= require('child_process').exec;
var Promise		= require("bluebird");

var currentProject = process.argv[2];

/**
 * Check to see if user has obmited project argument
 */
if(!currentProject) {
	console.log('');

	// Make sure the config directory exists before trying to read files in it.
	if (!fs.existsSync('./configs')) {
		fs.mkdirSync('./configs');
	}

	var availableConfigs = fs.readdirSync('./configs');

	if (availableConfigs.length === 0) {
		console.log('   It looks like this is the first time you run this app.');
		console.log('   You need to create a project to get started.');
		console.log();
		console.log('   Create a new project by running ' + clc.cyan(process.argv[0] + ' ' + getLastPartOfPath(process.argv[1]) + ' your-project-name init' ));
		console.log();
		console.log('   You\'ll be writing the project name many times so you probably want to make it short');
		console.log('   and use ' + clc.magenta('maw') + ' rather than ' + clc.magenta('My-Awesome-Project') + ' as a project name.');
	} else {
		console.log('Please run me with a project name, like this: ' + clc.cyan(process.argv[0] + ' ' + getLastPartOfPath(process.argv[1]) + ' project-name' ) );
		console.log('');
		console.log('Available projects:');
		for (var i = 0; i < availableConfigs.length; i++) {
			if (availableConfigs[i].length > 3) {
				console.log('   '  + clc.yellow(availableConfigs[i].substring(0, availableConfigs[i].length - 3)));
			}
		}
		console.log('');
		console.log('Create a new project by running ' + clc.cyan(process.argv[0] + ' ' + getLastPartOfPath(process.argv[1]) + ' your-project-name init' ));
	}

	console.log('');

	process.exit();
}

/**
 * Init (create new config file) if user wants us to
 * If not - load config, or warn that config does not exist.
 */
var configFile = './configs/' + currentProject + '.js';
switch(process.argv[3]) {
	case 'init':
		if (fs.existsSync(configFile)) {
				console.log('');
				console.log('   ' + clc.red('Config file already exists') + ': ' + configFile);
				console.log('');
				console.log(clc.bgGreen.black('Either:'));
				console.log('   1) Remove the config file you want to create a new one');
				console.log('   2) Run the script as usual. How about ' + clc.cyan(process.argv[0] + ' ' + getLastPartOfPath(process.argv[1]) + ' ' + currentProject + ' help'));
				console.log('');
				process.exit();
		} else {
			console.log('Running ' + clc.cyan('init'));

			// Make sure config folder is there
			fs.mkdirsSync('./configs');

			// Copy default file
			fs.copySync('config.default.js', configFile);

			console.log('');
			console.log('   ' + clc.green('Created Config File!'));
			console.log('');
			console.log(clc.bgGreen.black('Now go ahead and:'));
			console.log('   1) Edit the config: ' + clc.cyan(configFile));
			console.log('   2) Run ' + clc.cyan(process.argv[0] + ' ' + getLastPartOfPath(process.argv[1]) + ' ' + currentProject + ' test') + ' to check all paths');

			process.exit();
		}

		break;
	default:
		if (!fs.existsSync(configFile)) {
			console.log('');
			console.log('   ' + clc.red('Config file doesn\'t exists') + ': ' + configFile);
			console.log('');
			console.log(clc.bgGreen.black('You probably want to:'));
			console.log('   Run ' + clc.cyan(process.argv[0] + ' ' + getLastPartOfPath(process.argv[1]) + ' ' + currentProject + ' init' ) + ' to create a new config file for project \'' + currentProject + '\'');
			console.log('');
			process.exit();
		}
}


/**
 * Load Config File
 */
var config = require(configFile);

/**
 * Set Theme Config File
 */
var themeConfigFile = config.path.source.themeRoot + '/config.movescript.js';

/**
 * Load Theme Config File if it Exists
 */
if (fs.existsSync(themeConfigFile)) {
	var themeConfig = require(themeConfigFile);
}

/**
 * Main Switch
 */
var shellCommand = "";
var child;
switch(process.argv[3]) {
	case 'clean':
        console.log('');
        console.log('Running ' + clc.cyan('clean'));
		console.log('');

        shellCommand = 'rm -r ' + config.path.server.liferay + '/temp/*';
		child = exec(shellCommand, function(error, stdout, stderr) {
			if (error !== null) {
				console.log('   ' + clc.red(error));
				console.log('   Temp might already be empty');
				console.log('');
			} else {
				console.log('   ' + clc.green('Temp Cleaned'));
				console.log('');
			}
		});

        shellCommand = 'rm -r ' + config.path.server.liferay + '/work/*';
		child = exec(shellCommand, function(error, stdout, stderr) {
			if (error !== null) {
				console.log('   ' + clc.red(error));
				console.log('   Work might already be empty');
				console.log('');
			} else {
				console.log('   ' + clc.green('Work Cleaned'));
				console.log('');
			}
		});


		break;
    case 'build':
        console.log('');
        console.log('Running ' + clc.cyan('build'));

		for (var i = 0; i < themeConfig.copyList.length; i++) {
			// Check to se if it's a glob, if so - copy files one by one.
			if (themeConfig.copyList[i].indexOf("*") > -1) {
				glob(config.path.server.themeRoot + '/' + themeConfig.copyList[i], '', function (err, files) {
					if (files.length > 0) {
						files.forEach(function(currentFile) {
							fs.copySync(currentFile, config.path.source.themeDiffs + currentFile.substring(config.path.server.themeRoot.length));
						});
					}
				});
			} else {
				fs.copySync(config.path.server.themeRoot + '/' + themeConfig.copyList[i], config.path.source.themeDiffs + '/' + themeConfig.copyList[i]);
			}
		}

        console.log('');
        console.log(clc.blue('Copied files from server to code'));
        console.log(clc.blue('Build started...'));
        console.log('Sit tight, build happening in the background');

		// TODO: Can we get this function to output the stdout without waiting for it all to be finished.
		// To allow the user to se the status of the build.
		child = exec('cd ' + config.path.source.themeRoot + ' && ' + config.settings.onBuild.command, function(error, stdout, stderr) {
			if (error !== null) {
				console.log(clc.red('exec error: ') + error);
			}

			console.log(stdout);

			console.log('');
			console.log('   ' + clc.green('Build complete'));
			console.log('');
		});


        break;
    case 'up':
        console.log('');
        console.log('Running ' + clc.cyan('up'));

        if (true === config.settings.onUp.startServer) {
			shellCommand =	shellCommand +	"tell application \\\"System Events\\\" to keystroke \\\"t\\\" using {command down}\n" +
											"tell application \\\"Terminal\\\" to do script \\\"" + config.path.server.liferay + "/bin/" + config.settings.onUp.startCommand + "\\\" in front window\n";
        }

        if (true === config.settings.onUp.runCssWatcher) {
			shellCommand =	shellCommand +	"tell application \\\"System Events\\\" to keystroke \\\"t\\\" using {command down}\n" +
											"tell application \\\"Terminal\\\" to do script \\\"cd " + config.path.cssWatcher + "\\\" in front window\n" +
											"tell application \\\"Terminal\\\" to do script \\\"grunt\\\" in front window\n";
        }

        if (true === config.settings.onUp.tailCatalina) {
			shellCommand =	shellCommand +	"tell application \\\"System Events\\\" to keystroke \\\"t\\\" using {command down}\n" +
											"tell application \\\"Terminal\\\" to do script \\\"cd " + config.path.server.liferay + "/logs\\\" in front window\n" +
											"tell application \\\"Terminal\\\" to do script \\\"tail -f catalina.out\\\" in front window\n";
        }

        if (true === config.settings.onUp.openIde) {
			shellCommand =	shellCommand +	"tell application \\\"System Events\\\" to keystroke \\\"t\\\" using {command down}\n" +
											"tell application \\\"Terminal\\\" to do script \\\"" + config.settings.ide.command + " " + config.settings.ide.projectArgument + " " + config.file.ideProject + "\\\" in front window\n";
        }

        require('child_process').exec(osaWrap(shellCommand), function (error, stdout, stderr) { console.log(error); });

        break;
    case 'down':
		console.log('');
		console.log('Running ' + clc.cyan('down'));

        if (true === config.settings.onUp.startServer) {
			shellCommand = "tell application \\\"Terminal\\\" to do script \\\"" + config.path.server.liferay + "/bin/" + config.settings.onUp.stopCommand + "\\\" in front window\n";
        }

        require('child_process').exec(osaWrap(shellCommand), function (error, stdout, stderr) { console.log(error); });

        break;
    case 'test':
		console.log('');
		console.log('Running ' + clc.cyan('test'));
		console.log('');

		console.log('Checking ' + clc.blue('files'));
		console.log(fileExistPrint(configFile, 'Config File'));
		console.log(fileExistPrint(themeConfigFile, 'Theme Config File'));
		console.log('');

		console.log('Checking ' + clc.blue('Settings') + ' Set in (' + configFile + ')');
		console.log(booleanPrint(config.settings.onUp.openIde, '.settings.onUp.openIde'));
		console.log(booleanPrint(config.settings.onUp.runCssWatcher, '.settings.onUp.runCssWatcher'));
		console.log(booleanPrint(config.settings.onUp.tailCatalina, '.settings.onUp.tailCatalina'));
		console.log(booleanPrint(config.settings.onUp.startServer, '.settings.onUp.startServer'));
		console.log(configVariablePrint(config.settings.onUp.startCommand, '.settings.onUp.startCommand'));
		console.log(configVariablePrint(config.settings.onUp.stopCommand, '.settings.onUp.stopCommand'));
		console.log(configVariablePrint(config.settings.onBuild.command, '.settings.onBuild.command'));
		console.log(configVariablePrint(config.settings.ide.projectArgument, '.settings.ide.projectArgument'));
		console.log(configVariablePrint(config.settings.ide.command, '.settings.ide.command'));
		console.log('');

		console.log('Checking ' + clc.blue('Paths') + ' Set in (' + configFile + ')');
		console.log(fileExistPrint(config.path.server.liferay, '.path.server.liferay'));
		console.log(fileExistPrint(config.path.server.deploy, '.path.server.deploy'));
		console.log(fileExistPrint(config.path.source.themeRoot, '.path.source.themeRoot'));
		console.log(fileExistPrint(config.path.source.themeDiffs, '.path.source.themeDiffs'));
		console.log(fileExistPrint(config.path.server.themeRoot, '.path.server.themeRoot'));
		console.log(fileExistPrint(config.file.ideProject, '.file.ideProject'));
		console.log(fileExistPrint(config.path.cssWatcher, '.path.cssWatcher'));
		console.log('');

		console.log('Checking ' + clc.blue('Files to Copy') + ' Set in (' + themeConfigFile + ')');
		if (!fs.existsSync(themeConfigFile)) {
			console.log('   ' + 'Can not test because I couldn\'t find the Theme Config File: ');
			console.log('   ' + clc.red(themeConfigFile));
			console.log();
			console.log('   You probably want to either:');
			console.log('   a) Pull the ' + clc.cyan('config.movescript.js') + ' from your source code repository');
			console.log('   or');
			console.log('   b) Run ' + clc.cyan(process.argv[0] + ' ' + getLastPartOfPath(process.argv[1]) + ' ' + currentProject + ' init-theme') + ' to create a new Theme Config File');
		} else {
			themeConfig.copyList.forEach(function(entry) {
				var currentCopy = config.path.server.themeRoot + '/' + entry;
				// Check to see if it's a Glob, if so, use an alternative method of checking if file exists.
				if (currentCopy.indexOf("*") > -1) {
					glob(currentCopy, '', function (err, files) {
						if (files.length > 0) {
							console.log('    ' + clc.green('\u2713') + '   ' + pad(entry) + '    ' + clc.green(currentCopy));
							/* Can be uncommented to show all files in glob */
							// files.forEach(function(currentFile) {
							//	console.log('                                          ' + clc.green(getLastPartOfPath(currentFile)));
							// });
						} else {
							console.log(fileExistPrint(currentCopy, entry));
						}
						
					});

				} else {
					console.log(fileExistPrint(currentCopy, entry));
				}
			});
		}
        break;
    case 'init-theme':
		console.log('');
		console.log('Running ' + clc.cyan('init-theme'));

		if (fs.existsSync(themeConfigFile)) {
				console.log('');
				console.log('   ' + clc.red('Config file already exists') + ': ' + themeConfigFile);
				console.log('');
				console.log(clc.bgGreen.black('Either:'));
				console.log('   1) Remove the theme config file you want to create a new one');
				console.log('   2) Run the script as usual. How about ' + clc.cyan(process.argv[0] + ' ' + getLastPartOfPath(process.argv[1]) + ' ' + currentProject + ' help'));
				console.log('');
				process.exit();
		} else {
			// Copy default file
			fs.copySync('config.movescript.default.js', themeConfigFile);

			console.log('');
			console.log('   ' + clc.green('Created Theme Config File!'));
			console.log('');
			console.log(clc.bgGreen.black('Now go ahead and:'));
			console.log('   1) Edit ' + clc.cyan(themeConfigFile));
			console.log('   2) Run ' + clc.cyan(process.argv[0] + ' ' + getLastPartOfPath(process.argv[1]) + ' ' + currentProject + ' test') + ' to check all paths');
			console.log('');
			process.exit();
		}

		console.log('');
		break;
    default:
        console.log('');
		console.log('Please give me an argument, like this: ' + clc.cyan(process.argv[0] + ' ' + getLastPartOfPath(process.argv[1]) + ' ' + currentProject + ' up' ) );
		console.log('');
		console.log('Arguments:');
		console.log('    ' + clc.yellow('up') + '          - Start everything you\'ve set to start (server/IDE/tail log/grunt watcher)');
		console.log('    ' + clc.yellow('down') + '        - Stop server');
		console.log('    ' + clc.yellow('build') + '       - Copy files from server to code and build the theme');
		console.log('    ' + clc.yellow('clean') + '       - Clean the /temp and /work directories on the server');
		console.log('');
		console.log('    ' + clc.yellow('init') + '        - Create a config file for a project');
		console.log('    ' + clc.yellow('init-theme') + '  - Create a theme config file for a project');
		console.log('    ' + clc.yellow('test') + '        - Check the config files (and check that paths exists)');


}

// ------------------------------------------------------------------------
// MISC FUNCTIONS
// ------------------------------------------------------------------------

function getLastPartOfPath(path) {
	path = path.split('/');
	path = path[path.length -1];
	return path;
}

function fileExistPrint(filePath, fileName) {
	var out;
	if (fs.existsSync(filePath)) {
		out =  '    ' + clc.green('\u2713') + '   ' + pad(fileName) + '    ' + clc.green(filePath);
	} else {
		out =  '    ' + clc.red('\u2717') + '   ' + pad(fileName) + '    ' + clc.red(filePath);
	}

	if ( !filePath ) {
		out = out + clc.red('Not set');
	}

	return out;
}

function booleanPrint(bolToTest, booleanName) {
	var out;

	if ( typeof bolToTest === 'boolean' ) {
		out =  '    ' + clc.green('\u2713');
		out = out + '   ' + pad(booleanName, 30);
		if (bolToTest) {
			out = out + clc.green(bolToTest);
		} else {
			out = out + clc.magenta(bolToTest);
		}
	} else {
		out =  '    ' + clc.red('\u2717');
		out = out + '   ' + pad(booleanName, 30);
		out = out + clc.red('(not set or not true/false)');
	}

	return out;
}

function configVariablePrint(confVariable, confName) {
	var out;
	if ( confVariable ) {
		out =  '    ' + clc.green('\u2713');
		out = out + '   ' + pad(confName, 30);
		out = out + clc.white(confVariable);
	} else {
		out =  '    ' + clc.red('\u2717');
		out = out + '   ' + pad(confName, 30);
		out = out + clc.red('(not set)');
	}
	return out;
}

function osaWrap(str) {
	return 'osascript -e \"' + str + '\"';
}

function pad(str, len, dir, padding) {
    if (typeof(len) == "undefined") { len = 26; }
    if (typeof(padding) == "undefined") { padding = ' '; }
    if (typeof(dir) == "undefined") { dir = 'right'; }
    if (len + 1 >= str.length) {
        switch (dir){
            case 'left':
                str = Array(len + 1 - str.length).join(padding) + str;
				break;
            case 'both':
                var right = Math.ceil((padlen = len - str.length) / 2);
                var left = padlen - right;
                str = Array(left+1).join(padding) + str + Array(right+1).join(padding);
				break;
            default:
                str = str + Array(len + 1 - str.length).join(padding);
				break;
        }
    }
    return str;
}

